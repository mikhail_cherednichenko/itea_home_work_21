/* 
1. Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.
*/

const [...charaptersInputs] = document.querySelectorAll("form#charapters input");

charaptersInputs.forEach((el) => {
    el.addEventListener("change", (e) => {
        if (e.target.id === "dynamic-input") {
            validateCharapters(/^[\s\S]{1,16}$/g, e.target);
        } else if (e.target.id === "static-input") {
            validateCharapters(/^[\s\S]{8}$/g, e.target);
        } else if (e.target.id === "more-input") {
            validateCharapters(/^[\s\S]{5,}$/g, e.target);
        };
    });
});

function validateCharapters (p, v) {
    if (p.test(v.value)) {
        addValid(v);
    } else {
        addError(v);
    };
};

function addError(e) {
    e.classList.add("error");
    e.classList.remove("valid");
};

function addValid(e) {
    e.classList.add("valid");
    e.classList.remove("error");
};


/*
2.
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

const priceInput = document.getElementById("price");
const priceCurrect = document.getElementById("price__currect");

priceInput.onblur = () => {
    if (priceInput.value < 0 || !validate(/^\d+$/g, priceInput.value)) {
        priceCurrect.classList.add("visible__currect");
        priceInput.classList.add("error");
    } else {
        const div = document.createElement('div');
        const btn = document.createElement('span');

        priceInput.before(div);

        div.innerHTML = `<span class="prices">${priceInput.value}</span>`;
        div.appendChild(btn);
        div.classList.add('price__item');
    
        btn.innerText = "X";
        btn.classList.add('closeBtn');
        btn.addEventListener("click", () => {
            btn.parentNode.remove();
        });

        priceInput.value = "";
        
        priceCurrect.classList.remove("visible__currect");
        priceInput.classList.remove("error");
    };
};

const validate = (p, v) => p.test(v);